import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
// import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  eventId;
  event: any;
  scannedCode = null;
  attendance;
  events;
  attendanceId;
  isClaimed = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner, public http: Http, ) {
  }

    ionViewDidEnter(){
        // this.eventId = this.navParams.get('eventId');
        // console.log(this.eventId + "1");
        // this.http.get('http://13.228.23.48/fyp1admin/public/api/getEventDetails/' + this.navParams.get('eventId')).
        //         map(res => res.json()).
        //         subscribe(data => {
        //           console.log(data);
        //           this.event = data;
        //         });

        this.eventId = this.navParams.get('attendanceId');
        console.log(this.eventId + "1");
        this.http.get('http://13.228.23.48/fyp1admin/public/api/attendance/getAttendanceDetails/' + this.navParams.get('attendanceId')).
                map(res => res.json()).
                subscribe(datas => {
                  console.log(datas);
                  this.attendance = datas[0];
                  this.event = datas[1];
                  console.log(datas[0].ticketQuantity);
                  console.log(datas[1].eventName);
                }); 
    }

    claimTicket(attendanceId){
      this.http.get('http://13.228.23.48/fyp1admin/public/api/attendance/claimAttendance/'+attendanceId).
                map(res => res.json()).
                subscribe(datas => {
                  this.isClaimed = true;
                }); 

    }

}

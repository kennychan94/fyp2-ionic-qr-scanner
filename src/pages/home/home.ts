// import { Component } from '@angular/core';
// import { NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DetailsPage } from '../details/details';
// import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
 
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  qrData = null;
  createdCode = null;
  scannedCode = null;
  eventid;
  event: any;
  attendanceId;

  constructor(private barcodeScanner: BarcodeScanner,
      public navCtrl: NavController,
      public navParams: NavParams,
      public http: Http, 
      public alertCtrl: AlertController,) {

        // this.eventid = this.navParams.get('eventid');
        // this.http.get('http://13.228.23.48/fyp1admin/public/api/getEventDetails/' + this.navParams.get('eventid')).
        // map(res => res.json()).
        // subscribe(data => {
        //   console.log(data);
        //   this.event = data;
        // });

  }
 
  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
      this.navCtrl.push(DetailsPage,{
        attendanceId: this.scannedCode
      });
      // this.navCtrl.push(DetailsPage, this.scannedCode);
      console.log(this.scannedCode);
    }, (err) => {
        console.log('Error: ', err);
    });
    //console.log(this.scannedCode);
    // this.navCtrl.push(DetailsPage, );
    
  }

  // scanCode(eventid){
  //   this.navCtrl.push(DetailsPage,{
  //     eventid: eventid,
  //   });
  // }
 
}